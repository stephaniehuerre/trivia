//
//  Trivia.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

import Foundation

enum Trivia {
  
}

extension Trivia {
  struct SegueId {
    static let goToGameSegue: String = "goToGameSegue"
    static let stopGameSegue: String = "stopGameSegue"
  }
}

