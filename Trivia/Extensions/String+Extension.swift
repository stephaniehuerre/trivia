//
//  String+Extension.swift//
//  NovAccess
//
//  Created by Stephanie Huerre on 24/01/2019.
//  Copyright © 2019 My Jungly. All rights reserved.
//

import Foundation
import UIKit

extension String {

  // a valid name can't be empty
  func isValidName() -> Bool {
    return self.length > 0
  }
  
  var length: Int {
    return count
  }
  
  func displayHTMLString() -> String {
    guard let data = self.data(using: .utf8) else {
        return self
    }

    let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
        .documentType: NSAttributedString.DocumentType.html,
        .characterEncoding: String.Encoding.utf8.rawValue
    ]

    guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
        return self
    }
    return attributedString.string
  }

  init?(htmlEncodedString: String) {

      guard let data = htmlEncodedString.data(using: .utf8) else {
          return nil
      }

      let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
          .documentType: NSAttributedString.DocumentType.html,
          .characterEncoding: String.Encoding.utf8.rawValue
      ]

      guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
          return nil
      }

      self.init(attributedString.string)

  }
}
