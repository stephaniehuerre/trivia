//
//  Game.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

import Foundation

class Game: NSObject {
  // MARK: - public vars
  let nbQuestions = 10
  
  // MARK: - private vars
  private(set) var questions: [Question]? = nil
  private(set) var player1: Player? = nil
  private(set) var player2: Player? = nil
  
  // MARK: - private session vars
  private var questionsTask: URLSessionDataTask?
  private var session: URLSession {
    return URLSession.shared
  }
  
  // MARK: - public functions
  func setPlayers(player1: Player, player2: Player) {
    self.player1 = player1
    self.player2 = player2
  }
}

// MARK: - API related functinos
extension Game {
  // MARK: - API Response Code
  enum APIResponseCode: Int, Codable {
    case success = 0
    case noResult = 1
    case invalidParameter = 2
    case tokenNotFound = 3
    case tokenEmpty = 4
    
    func message() -> String {
      switch self {
        case .success:
          return "Succesful API request"
        case .noResult:
          return "No result on API request"
        case .invalidParameter:
          return "Invalid parameter"
        case .tokenNotFound:
          return "Token Not Found"
        case .tokenEmpty:
          return "Empty token"
      }
    }
  }

  // MARK: - APIResult
  struct APIResult: Codable {
    let responseCode: APIResponseCode
    let results: [Question]
    
    private enum CodingKeys: String, CodingKey {
      case responseCode = "response_code"
      case results
    }
  }
  
  // MARK: - public func
  // loads questions from API
  // documentation available here: https://opentdb.com/api_config.php
  func loadQuestion(completion: @escaping (String?) -> Void)  {
    let urlString = "https://opentdb.com/api.php?amount=\(nbQuestions)&type=multiple"
    print("FETCH URL = \(urlString)")
    
    guard let url = URL(string: urlString) else {
      completion("API url error : \(urlString)")
      return
    }
    
    if let task = questionsTask, task.taskIdentifier > 0 && task.state == .running {
      task.cancel()
    }
    
    questionsTask = session.dataTask(with: url) { data, response, error in
      guard error == nil else {
        DispatchQueue.main.async {
          completion("error in fetching questions : \(error!.localizedDescription)")
        }
        return
      }
      
      guard let data = data,
            let apiResults = try? JSONDecoder().decode(APIResult.self, from: data) else {
        DispatchQueue.main.async {
          completion("API result decoding error")
        }
        return
      }
      defer {
        DispatchQueue.main.async {
          completion(nil)
        }
      }
      self.questions = apiResults.results
    }
    questionsTask?.resume()
  }
}
