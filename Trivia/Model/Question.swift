//
//  Question.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

import Foundation


// MARK: - Question structure
struct Question: Codable {
  let title: String
  let incorrectAnswers: [String]
  let correctAnswer: String
  let category: String
  
  private enum CodingKeys: String, CodingKey {
    case title = "question"
    case incorrectAnswers = "incorrect_answers"
    case correctAnswer = "correct_answer"
    case category
  }
}
