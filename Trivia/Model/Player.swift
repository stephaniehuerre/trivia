//
//  Player.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

class Player {
  // MARK: - private vars
  private(set) var name: String
  private(set) var currentScore: Int = 0
  
  // MARK: - initializers
  init(name: String) {
      self.name = name
  }
  
  // MARK: - public functions
  public func increaseScore(amount: Int) {
    currentScore += amount
  }
}

// MARK: - Equatable
extension Player: Equatable {
  public static func == (lPlayer: Player, rPlayer: Player) -> Bool {
    return lPlayer.name == rPlayer.name
  }
}
