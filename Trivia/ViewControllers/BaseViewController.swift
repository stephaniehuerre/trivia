//
//  BaseViewController.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

import UIKit

class BaseViewController: UIViewController {

  // MARK: - public func
  func showPopUp(title: String? = nil, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
    DispatchQueue.main.async {
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
      self.present(alert, animated: true)
    }
  }
}

