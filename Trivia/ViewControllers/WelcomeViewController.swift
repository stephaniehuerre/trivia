//
//  WelcomeViewController.swift
//  Trivia
//
//  Created by steph on 12/17/20.
//

import UIKit

class WelcomeViewController: BaseViewController {

  // MARK: - private vars
  private var game: Game = Game()
  
  // MARK: - ibOutlets
  @IBOutlet weak var name1TextField: UITextField!
  @IBOutlet weak var name2TextField: UITextField!
  @IBOutlet weak var startButton: UIButton!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  // MARK: - ibActions
  @IBAction func didPressTextFieldDone(_ sender: UITextField) {
    sender.resignFirstResponder()
  }
  
  @IBAction func stopGame(segue: UIStoryboardSegue) {
    resetGame()
    view.endEditing(true)
  }
  
  // MARK: - UIViewController overrides
  override func viewDidLoad() {
    super.viewDidLoad()
    loadGameQuestions()
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    if identifier == Trivia.SegueId.goToGameSegue {
//          guard var _ = game else {
//            showPopUp(message: "no game loaded")
//              return false
//          }
          guard updatePlayerNames() else {
            showPopUp(message: "Fill players names, they must be different")
            return false
          }
      }
      return true
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
    if segue.identifier == Trivia.SegueId.goToGameSegue,
       let gameViewController = segue.destination as? GameViewController {
//      guard let game = game else {
//        print("no game existing")
//        return
//      }
      gameViewController.game = game
    }
  }
  
  // MARK: - private functions
  
  // set the game player names
  // return false if names are not valid
  private func updatePlayerNames() -> Bool {
    guard let name1 = name1TextField.text,
          let name2 = name2TextField.text,
          name1.isValidName(),
          name2.isValidName(),
          name1 != name2
//          let game = game
    else {
      return false
    }
    game.setPlayers(player1: Player(name: name1),
                    player2: Player(name: name2))
    return true
  }
  
  private func loadGameQuestions() {
    self.activityIndicator.startAnimating()
    self.startButton.isEnabled = false
    
    game.loadQuestion(completion: { errorString in
      guard errorString == nil else {
        self.showPopUp(title: "Error", message: errorString!)
        return
      }
      DispatchQueue.main.async {
        print("game questions loaded successfully")
        self.activityIndicator.stopAnimating()
        self.startButton.isEnabled = true
      }
    })
  }
  
  private func resetGame() {
    loadGameQuestions()
    name1TextField.text = nil
    name2TextField.text = nil
  }
}
