//
//  GameViewController.swift
//  Trivia
//
//  Created by steph on 12/18/20.
//

import UIKit

class GameViewController: BaseViewController {
  // MARK: - private vars
  var game: Game
  
  private var currentPlayer: Player? {
    didSet {
      updateCurrentPlayerUI()
    }
  }
  
  private var currentQuestionId: Int = 0 {
    didSet {
      guard currentQuestionId < game.nbQuestions else {
        endGame()
        return
      }
      udpateQuestionUI()
      nextPlayersTurn()
    }
  }
  
  private var currentQuestion: Question? {
    return game.questions?[currentQuestionId]
  }
  
  private var correctAnswerId: Int = 0
  
  // MARK: - ibOutlets
  @IBOutlet weak var player1NameLabel: UILabel!
  @IBOutlet weak var player2NameLabel: UILabel!
  @IBOutlet weak var player1ScoreLabel: UILabel!
  @IBOutlet weak var player2ScoreLabel: UILabel!
  @IBOutlet weak var currentPlayerNameLabel: UILabel!
  @IBOutlet weak var questionLabel: UILabel!
  @IBOutlet var answerButtons: [UIButton]!
  
  
  // MARK: - ibActions
  @IBAction func didSelectAnswer(_ sender: UIButton) {
    let completionHandler = { self.currentQuestionId += 1 }
    guard sender.tag == correctAnswerId else {
      showPopUp(message: "WRONG ANSWER") { _ in
        completionHandler()
      }
      return
    }
    
    showPopUp(message: "CORRECT ANSWER") { _ in
      if let player = self.currentPlayer {
        player.increaseScore(amount: 1)
        self.updatePlayerScoresUI()
      }
      completionHandler()
    }
  }
  
  // MARK: - initializers
  init(imageURL: NSURL?) {
    self.game = Game()
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    self.game = Game()
    super.init(coder: coder)
  }
  
  // MARK: - UIViewController overrides
  override func viewDidLoad() {
    super.viewDidLoad()
    currentPlayer = game.player1
    updateUI()
  }
  
  // MARK: - private UI update functions
  private func updateUI() {
    updatePlayerNamesUI()
    updatePlayerScoresUI()
    updateCurrentPlayerUI()
    udpateQuestionUI()
  }
  
  private func updateCurrentPlayerUI() {
    currentPlayerNameLabel.text = currentPlayer?.name
  }
  
  private func updatePlayerNamesUI() {
    guard let player1 = game.player1,
          let player2 = game.player2 else {
      stopGameWithPopup(message: "Stopping game: No players in game")
      return
    }
    player1NameLabel.text = player1.name
    player2NameLabel.text = player2.name
  }
  
  private func updatePlayerScoresUI() {
    guard let player1 = game.player1,
          let player2 = game.player2 else {
      stopGameWithPopup(message: "Stopping game: No players in game")
      return
    }
    player1ScoreLabel.text = "\(player1.currentScore)"
    player2ScoreLabel.text = "\(player2.currentScore)"
  }
  
  private func udpateQuestionUI() {
    questionLabel.text = currentQuestion?.title.displayHTMLString()
    
    correctAnswerId = Int.random(in: 0..<4)
    answerButtons[correctAnswerId].setTitle("\(currentQuestion!.correctAnswer.displayHTMLString())",
                                            for: .normal)
    answerButtons[correctAnswerId].tag = correctAnswerId
    print("question: \(currentQuestion!.title.displayHTMLString())")
    print("correct answer: \(currentQuestion!.correctAnswer.displayHTMLString())")
    
    guard let incorrectAnswers = currentQuestion?.incorrectAnswers else {
      return
    }
    
    var currentAnswerId = 0
    incorrectAnswers.forEach {
      if currentAnswerId == correctAnswerId {
        currentAnswerId += 1
      }
      let answer = "\($0)"
      answerButtons[currentAnswerId].setTitle(answer.displayHTMLString(), for: .normal)
      answerButtons[currentAnswerId].tag = currentAnswerId
      currentAnswerId += 1
    }
  }
  
  private func stopGameWithPopup(message: String) {
    showPopUp(message: message) { _ in
      self.performSegue(withIdentifier: Trivia.SegueId.stopGameSegue, sender: nil)
    }
  }
  
  // MARK: - private game management functions
  private func nextPlayersTurn() {
    guard let player1 = game.player1,
          let player2 = game.player2 else {
      stopGameWithPopup(message: "Stopping game: No players in game")
      return
    }
    currentPlayer = (currentPlayer == player1) ? player2 : player1
  }
  
  private func endGame() {
    guard let player1 = game.player1,
          let player2 = game.player2  else {
      stopGameWithPopup(message: "Stopping game: No players in game")
      return
    }
    
    var winnerMessage = ""
    if player1.currentScore == player2.currentScore {
      winnerMessage = "It's a tie!"
    } else {
      let winner = player1.currentScore > player2.currentScore ? player1 : player2
      winnerMessage = "Congrats \(winner.name)! You are the winner"
    }
    stopGameWithPopup(message: winnerMessage)
  }
}

